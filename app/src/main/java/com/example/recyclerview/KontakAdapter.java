package com.example.recyclerview;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class KontakAdapter extends RecyclerView.Adapter<KontakAdapter.VH> {

    private ArrayList<Kontak> listkontak;

    public KontakAdapter(ArrayList<Kontak> listkontak) {
        this.listkontak = listkontak;
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cetakan_kontan,parent,false);
        return new VH(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        holder.bind(listkontak.get(position));

    }

    @Override
    public int getItemCount() {
        return listkontak.size();
    }

    public class VH extends RecyclerView.ViewHolder {
        TextView tv_Nama;
        ImageView iv_Foto;


        public VH(@NonNull View itemView) {
            super(itemView);

            tv_Nama = itemView.findViewById(R.id.tv_nama);
            iv_Foto = itemView.findViewById(R.id.iv_foto);
        }

        public void bind(final Kontak data){
            tv_Nama.setText(data.getNama());
            Glide.with(itemView.getContext()).load(data.getFoto()).into(iv_Foto);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(itemView.getContext(),data.getNama(),Toast.LENGTH_SHORT).show();
                    Intent pindah = new Intent(itemView.getContext(),DetailActivity.class);
                    pindah.putExtra(DetailActivity.EXTRA_DATA,data);
                    itemView.getContext().startActivity(pindah);
                }
            });


        }
    }

}
