package com.example.recyclerview;

import java.util.ArrayList;

public class KontakData {

    public static String nama[] = new String[]{

            "Rivanka Faawwas Assyraaj",
            "Tanaka Yuu",
            "Bokuto Kotaro",
            "Kageyama Tobio",
            "Nishinoya Yuu",
            "Hinata Shouyo",
            "Miya atsumu",
            "Miya Osamu",
            "Kuro my baby",
            "daichi sawamura"

    };

    public static String nomorhp[] = new String[]{

            "082136564484",
            "082241615847",
            "082223467889",
            "081245875476",
            "083456542323",
            "081243657890",
            "084507654321",
            "087634558900",
            "087654332256",
            "087357899222"
    };

    public static int poto[] = new int[]{

            R.drawable.user1,
            R.drawable.user2,
            R.drawable.user3,
            R.drawable.user4,
            R.drawable.user5,
            R.drawable.user6,
            R.drawable.user7,
            R.drawable.user8,
            R.drawable.user9,
            R.drawable.user10
    };
    public static ArrayList<Kontak> getisData(){
         ArrayList<Kontak> list =new ArrayList<>();
         for (int i=0; i<nama.length; i++){
            Kontak sim = new Kontak();
            sim.setNama(nama[i]);
            sim.setNomorhp(nomorhp[i]);
            sim.setFoto(poto[i]);

            list.add(sim);

         }
         return list;
    }
}

