package com.example.recyclerview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class DetailActivity extends AppCompatActivity {
    TextView tv_Nama;
    TextView tv_Nohp;
    ImageView iv_Poto;

    public  static final String EXTRA_DATA = "EKTRA_DATA";
    private Kontak tangkep;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        tangkep = getIntent().getParcelableExtra(EXTRA_DATA);

        tv_Nama = findViewById(R.id.tv_nama);
        tv_Nohp = findViewById(R.id.tv_nohp);
        iv_Poto = findViewById(R.id.iv_poto);

        tv_Nama.setText(tangkep.getNama());
        Glide.with(getApplicationContext()).load(tangkep.getFoto()).into(iv_Poto);
        tv_Nohp.setText(tangkep.getNomorhp());

    }
}