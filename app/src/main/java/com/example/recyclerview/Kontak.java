package com.example.recyclerview;

import android.os.Parcel;
import android.os.Parcelable;


public class Kontak implements Parcelable {

    private String nama;
    private String nomorhp;
    private int foto;

    public Kontak() {

    }

    protected Kontak(Parcel in) {
        nama = in.readString();
        nomorhp = in.readString();
        foto = in.readInt();
    }

    public static final Creator<Kontak> CREATOR = new Creator<Kontak>() {
        @Override
        public Kontak createFromParcel(Parcel in) {
            return new Kontak(in);
        }

        @Override
        public Kontak[] newArray(int size) {
            return new Kontak[size];
        }
    };

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNomorhp() {
        return nomorhp;
    }

    public void setNomorhp(String nomorhp) {
        this.nomorhp = nomorhp;
    }

    public int getFoto() {
        return foto;
    }

    public void setFoto(int foto) {
        this.foto = foto;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nama);
        dest.writeString(nomorhp);
        dest.writeInt(foto);
    }
}


