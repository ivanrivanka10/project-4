package com.example.recyclerview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RecyclerView rvItem;
    ArrayList<Kontak> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rvItem = findViewById(R.id.rv_item);

        list.addAll(KontakData.getisData());

        rvItem.setLayoutManager(new LinearLayoutManager(this));
        KontakAdapter kontakadapter = new KontakAdapter(list);
        rvItem.setAdapter(kontakadapter);

    }
}
